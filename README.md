### Pythonic Idioms
The examples in src/idioms illustrate various programming idioms that may be
considered "pythonic".

They are largely based on those proposed by José Javier Merchante Picazo:
https://github.com/jjmerchante/memoriaTFG-JoseJavierMerchantePicazo/blob/master/memoria.pdf

