#!/usr/bin/env python3
# -*- coding: utf-8 -*-

class ClassA(object):
  def __init__(self, argA):
    self.varA = argA
  def funB(self):
    return 2

class ClassB(ClassA):
  def funB(self):
    varB = 1
    def funInner(x):
      return x + 1
    return funInner(varB)

class ClassC(object):
  def funC(self):
    pass

class ClassD(ClassB, ClassC):
  def funD(self):
    pass

