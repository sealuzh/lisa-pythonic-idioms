#!/usr/bin/env python3
# -*- coding: utf-8 -*-

names = ["John", "Alexander", "Bob"]
marks = [5.5, 7, 10]
# should be using zip
for n in range(len(names)):
    print(names[n], marks[n])

