#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# should be using enumerate()
seasons = ['Spring', 'Summer', 'Fall', 'Winter']
n = 1
for s in seasons:
    print((n, s))
    n += 1

