#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# should be using with statement
f = open('/tmp/with.py.txt', 'w')
f.write('Hello, World')
f.close()

def scoped():
    f = open('/tmp/with.py.txt', 'a')
    f.write('Hello, World')
    f.close()
scoped()

