#!/usr/bin/env python3
# -*- coding: utf-8 -*-

numbers = range(10)
print(list(filter(lambda x: x % 2, numbers)))
# [1, 3, 5, 7, 9]

