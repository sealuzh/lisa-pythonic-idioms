#!/usr/bin/env python3
# -*- coding: utf-8 -*-

def createSquares(untilNum):
    list = range(untilNum+1)
    for i in list:
        yield i**2

squaresUntil10 = createSquares(10)
for i in squaresUntil10:
    print(i)
# 0 1 4 9 16 25 36 49 64 81 100

