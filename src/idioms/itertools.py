#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# zip_longest
from itertools import zip_longest
names = ["John", "Alexander", "Bob", "Alice"]
marks = [5.5, 7, 10]
for name, mark in zip_longest(names, marks, fillvalue="absent"):
    print(name, mark)

# starmap
from itertools import starmap
items = [(1, 7), (2, 8), (3, 9)]
res = starmap(lambda x, y: x*y, items)
print(list(res)) # res is an iterator
# [7, 16, 27]

# tee
from itertools import tee
it = range(10)
it1, it2, it3 = tee(it, 3)
print(next(it1)) # 0

# groupby
from itertools import groupby
# Show the words with the same initial letter together
words = ["dog", "cat", "house", "car", "function", "class", "foo"]
# words MUST be sorted
words_sorted = sorted(words)
for key, group in groupby(words_sorted, lambda x: x[0]):
    print(list(group)) # group is iterator
