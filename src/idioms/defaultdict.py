#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from collections import defaultdict

dict_def = defaultdict(int)
dict_def['foo'] = dict_def['bar'] + 25
print(dict_def.items())  # [(’foo’, 25), (’bar’, 0)]

