#!/usr/bin/env python3
# -*- coding: utf-8 -*-

numbers = range(20)
print(list(filter(lambda x: x % 2, numbers)))

