#!/usr/bin/env python3
# -*- coding: utf-8 -*-

class Magic(object):
    def __init__(self):
        self.a = 'foo'

    # level 1
    def __getattr__(self, name):
        return "default"

    # level 2
    def __unicode__(self):
        return super().__unicode__()

    def __ror__(self, other):
        return super().__ror__(other)

    # level 3
    def __missing__(self, key):
        return super().__missing__(key)

t = Magic()
print(t.a)             # 'foo'
print(hasattr(t, 'b')) # True
print(t.b)             # 'default'
print(getattr(t, 'd')) # 'default'

