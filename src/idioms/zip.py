#!/usr/bin/env python3
# -*- coding: utf-8 -*-

names = ["John", "Alexander", "Bob"]
marks = [5.5, 7, 10]
for name, mark in zip(names, marks):
    print(name, mark)

