#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from collections import namedtuple

Point = namedtuple('Point', 'x y')
p = Point(5, 6)
print(p.x) # 5
print(p.y) # 6

