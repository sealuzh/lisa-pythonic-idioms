#!/usr/bin/env python3
# -*- coding: utf-8 -*-

squareNumbers = [x*x for x in range(10)]
print(squareNumbers)
# [0, 1, 4, 9, 16, 25, 36, 49, 64, 81]

