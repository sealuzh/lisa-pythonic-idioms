#!/usr/bin/env python3
# -*- coding: utf-8 -*-

numbers = range(5)
print(list(map(lambda x: x**2, numbers)))

