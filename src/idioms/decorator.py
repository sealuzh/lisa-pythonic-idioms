#!/usr/bin/env python3
# -*- coding: utf-8 -*-

def decorator1(fdecorated):
    def caller(*args, **kwargs):
        print("d1: Arguments are: %s, %s" % (args, kwargs))
        return fdecorated(*args, **kwargs)
    return caller

def decorator2(fdecorated):
    def caller(*args, **kwargs):
        print("d2: Arguments are: %s, %s" % (args, kwargs))
        dec = fdecorated(*args, **kwargs)
        return dec
    c = caller
    return c

@decorator1
@decorator2
def sum(a, b):
    return a + b

print(sum(1, 2))
# Arguments are: (1, 2), {}
# 3

