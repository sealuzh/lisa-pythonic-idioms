#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from collections import deque

seq = deque([1, 2, 3, 4, 5])
print(seq.pop()) # 5
seq.append('b')
print(seq.popleft()) # 1
seq.appendleft('a')
print(seq)
# deque(['a', 2, 3, 4, 'b'])

