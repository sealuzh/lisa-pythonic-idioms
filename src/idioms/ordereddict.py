#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from collections import OrderedDict

d = OrderedDict()
d['1'] = 'A'
d['2'] = 'B'
d['3'] = 'C'
d['1'] = 'D'
del(d['2'])
d['2'] = 'E'
for key, val in d.items():
    print(key + ':' + val)
#1:D
#3:C
#2:E

