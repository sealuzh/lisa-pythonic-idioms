#!/usr/bin/env python3
# -*- coding: utf-8 -*-

l = []

try:
    l[0]
    l.append("baz")
except IndexError:
    l.append("foo")
finally:
    l.append("bar")

print(l) # ['foo', 'bar']

