#!/usr/bin/env python3
# -*- coding: utf-8 -*-

sumSquareNumbers = sum(x**2 for x in range(5))
print(sumSquareNumbers) # 30

